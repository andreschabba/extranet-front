import React from 'react';

import { MX, US } from '../constants/constants';

const Flag = ({ country }) => {
  if (country === MX || country === US) {
    return (
      <div>
        <h1 className="ui center aligned header">
          {country === MX ? 'Bienvenido a México' : 'Welcome to United States'}
        </h1>
        <img
          className="ui fluid image"
          src={process.env.PUBLIC_URL + `/flags/${country}.png`}
          alt={country}
        />
      </div>
    );
  }
  return null;
};

export default Flag;
