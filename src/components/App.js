import React, { useEffect, useState } from 'react';

import api from '../API/api';
import { MX, US } from '../constants/constants';
import Flag from './Flag';

const App = () => {
  const [country, setCountry] = useState('');

  useEffect(() => {
    getCountry();
    // Tests
    // getCountry(US);
    // getCountry(MX);
  }, []);

  const getCountry = async (country = null) => {
    const endpoint = country || 'country';
    const response = await api.get(`/${endpoint}`);
    setCountry(response.data);
  };

  return (
    <div className="ui container" style={{ marginTop: '50px' }}>
      <Flag country={country} />
    </div>
  );
};

export default App;
